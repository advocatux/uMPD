/*
 * Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <QObject>
#include <QDebug>

#include "mpd/client.h"
#include "playlistmodel.h"

class PlayList : public QObject
{
  Q_OBJECT
  
  Q_PROPERTY(PlayListModel *playmodel READ playmodel WRITE setPlaymodel NOTIFY playmodelChanged)
  Q_PROPERTY(int selPlaylist READ selPlaylist WRITE setPlaylist NOTIFY selPlaylistChanged)
  
public:
  PlayList(QObject *parent = 0);

  void getConnection(struct mpd_connection *connection);
  
  void checkPlaylists(void);
  
  Q_INVOKABLE bool saveAs(QString playlistName);
  Q_INVOKABLE bool addToQueue();
  Q_INVOKABLE bool remove();
  Q_INVOKABLE void selectedEntity(QString playlistName, QString playlistNumber);
  
  void setPlaylist(int playlistNum);
  int selPlaylist() const;
  
  PlayListModel *playmodel();
  void setPlaymodel(PlayListModel *playmodel);
  
signals:
  void playmodelChanged(PlayListModel *playmodel);
  void selPlaylistChanged(int playlist);

private slots:
  
private:
  struct mpd_connection *m_connection;
  QString m_playlistName = "";
  int m_playlistNum = 0;

  PlayListModel *m_playmodel = nullptr;
};

#endif
