/*
 * Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "playercontrol.h"

PlayerControl::PlayerControl(QObject *parent) : QObject(parent)
{
}

void PlayerControl::getConnection(struct mpd_connection *connection)
{
  m_connection = connection;
}

void PlayerControl::setVolume(unsigned value)
{
  mpd_run_set_volume(m_connection, value);
  emit volumeChanged();
}

bool PlayerControl::playPrevious(void)
{
  return mpd_run_previous(m_connection);
}

bool PlayerControl::playNext(void)
{
  return mpd_run_next(m_connection);
}

bool PlayerControl::play(unsigned state)
{
  bool play;
  
  if( state == 2)
  {
    play = mpd_run_pause(m_connection, true);
  }
  else
  {
    play = mpd_run_play(m_connection);
  }
  emit playingChanged();
  
  return play;
}

bool PlayerControl::stop(void)
{
  bool stop = mpd_run_stop(m_connection);
  emit playingChanged();
  return stop;
}

bool PlayerControl::playPos(unsigned id)
{
  bool pos = mpd_run_play_pos(m_connection, id);
  emit songChanged();
  return pos;
}

void PlayerControl::playSeekPos(unsigned int id, float timePos)
{
  int t = timePos * 60;
  bool seekPos = mpd_run_seek_pos(m_connection, id, t);

  emit seekedPos();
}

bool PlayerControl::repeat(bool mode)
{
  bool repeat = mpd_run_repeat(m_connection, !mode);
  emit repeatChanged();
  return repeat;
}

bool PlayerControl::shuffle(bool mode)
{
  bool shuffle = mpd_run_random(m_connection, !mode);
  emit shuffleChanged();
  return shuffle;
}
