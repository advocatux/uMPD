/*
 * Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "playlist.h"

PlayList::PlayList(QObject *parent) : QObject(parent)
{
  auto playmodel = new PlayListModel(this);
  setPlaymodel(playmodel);
}

void PlayList::setPlaymodel(PlayListModel* playmodel)
{
  m_playmodel = playmodel;
}

PlayListModel * PlayList::playmodel()
{
  return m_playmodel;
}

void PlayList::getConnection(struct mpd_connection *connection)
{
  m_connection = connection;
  
  //Abfragen der gespeicherten Playlists
  checkPlaylists();
}

void PlayList::selectedEntity(QString playlistName, QString playlistNumber)
{
  qDebug() << "gewaehlte playlist:" << playlistName;
  if(playlistName != m_playlistName) {
    m_playlistName = playlistName;
    m_playlistNum = playlistNumber.toInt();
    setPlaylist(m_playlistNum);
  }
}

void PlayList::setPlaylist(int playlistNum)
{
  qDebug() << "aktuelle Nummer:" << playlistNum;
  emit selPlaylistChanged(playlistNum);
}

int PlayList::selPlaylist() const
{
  return m_playlistNum;
}

bool PlayList::saveAs(QString playlistName)
{
  if(playlistName != "") {
    QByteArray baqueueName = playlistName.toLocal8Bit();
    const char *name = baqueueName.data();

    bool stat = mpd_run_save(m_connection, name);
    checkPlaylists();
    return stat;
  } else {
    return false;
  }
}

bool PlayList::addToQueue()
{
  QByteArray baqueueName = m_playlistName.toLocal8Bit();
  const char *name = baqueueName.data();

  return mpd_run_load(m_connection, name);
}

bool PlayList::remove()
{
  QByteArray baqueueName = m_playlistName.toLocal8Bit();
  const char *name = baqueueName.data();

  bool stat = mpd_run_rm(m_connection, name);
  checkPlaylists();
  return stat;
}

void PlayList::checkPlaylists()
{
  qDebug() << "Abfrage: " << mpd_send_list_playlists(m_connection);
  const struct mpd_playlist *playlist;

  QStringList savedPlaylists;
  
  struct mpd_entity *entity;

  while (entity = mpd_recv_entity(m_connection)) {
      
    if (mpd_entity_get_type(entity) == MPD_ENTITY_TYPE_PLAYLIST)
    {
      playlist = mpd_entity_get_playlist(entity);
      savedPlaylists << mpd_playlist_get_path(playlist);
    }
      
    mpd_entity_free(entity);
    
  }
  
  m_playmodel->loadPlaylists(savedPlaylists);  
  
}
