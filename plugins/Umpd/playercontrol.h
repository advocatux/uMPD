/*
 * Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYERCONTROL_H
#define PLAYERCONTROL_H

#include <QObject>
#include <QDebug>

#include "mpd/client.h"

class PlayerControl : public QObject
{
  Q_OBJECT
  
public:
  PlayerControl(QObject *parent = 0);
  
  Q_INVOKABLE void setVolume(unsigned value);
  Q_INVOKABLE bool playPrevious(void);
  Q_INVOKABLE bool playNext();
  Q_INVOKABLE bool play(unsigned state);
  Q_INVOKABLE bool stop();
  Q_INVOKABLE bool playPos(unsigned id);
  Q_INVOKABLE void playSeekPos(unsigned id, float timePos);
  Q_INVOKABLE bool repeat(bool mode);
  Q_INVOKABLE bool shuffle(bool mode);

  void getConnection(struct mpd_connection *connection);
  
signals:
  void volumeChanged(void);
  void playingChanged(void);
  void songChanged(void);
  void repeatChanged(void);
  void shuffleChanged(void);
  void seekedPos(void);
  
private:
  struct mpd_connection *m_connection;
  
};

#endif
