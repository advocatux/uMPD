/*
 * Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYLISTMODEL_H
#define PLAYLISTMODEL_H

#include <QObject>
#include <QVariant>
#include <QAbstractListModel>
#include <QHash>
#include <QByteArray>
#include <QDebug>

class PlayListModel: public QAbstractListModel
{
    Q_OBJECT

public:    
    enum theRoles {textRole= Qt::UserRole + 1, otherTextRole};
    
    PlayListModel(QObject *parent = 0);
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    void appendRow(QString textData, QString otherTextData, QString anotherTextData);
    void myRemoveRows(int position, int rows);
    
    void addAnItem(QVariant textItem, QVariant otherTextItem, QVariant anotherTextItem);
    void removeLastItem();
    void removeAllItems();
    Q_INVOKABLE void createUriList();
    
    void loadQueue(QStringList uriList, QStringList titleList);
    void loadPlaylists(QStringList playlists);

protected :
    QHash<int, QByteArray> roleNames() const;

private slots:
    
private:
    QList <QString> m_textItems;
    QList <QString> m_otherTextItems;
    QList <QString> m_otherOtherItems;
};

#endif
