/*
 * Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYERSTATE_H
#define PLAYERSTATE_H

#include <QObject>
#include <QDebug>

#include "mpd/client.h"
#include "playlistmodel.h"

class PlayerState : public QObject
{
  Q_OBJECT
  Q_PROPERTY(PlayListModel *listmodel READ listmodel WRITE setListmodel NOTIFY listmodelChanged)
  
  Q_PROPERTY(double volume READ volume WRITE setVolume NOTIFY volumeChanged)
  Q_PROPERTY(unsigned playing READ playing WRITE setPlaying NOTIFY playingChanged)
  Q_PROPERTY(int song READ song WRITE setSong NOTIFY songChanged)
  Q_PROPERTY(bool repeat READ repeat WRITE setRepeat NOTIFY repeatChanged)
  Q_PROPERTY(bool shuffle READ shuffle WRITE setShuffle NOTIFY shuffleChanged)
  Q_PROPERTY(float elapsedTime READ elapsedTime WRITE setElapsedTime NOTIFY elapsedTimeChanged)
  Q_PROPERTY(float totalTime READ totalTime WRITE setTotalTime NOTIFY totalTimeChanged)
  
  PlayListModel *listmodel();
  void setListmodel(PlayListModel *listmodel);
  
public:
  PlayerState(QObject *parent = 0);

  Q_INVOKABLE void update(void);
  
  void getConnection(struct mpd_connection *connection);
  
  void setVolume(int value);
  double volume() const;
  void checkVolume(void);
  
  void setPlaying(unsigned state);
  unsigned playing() const;
  void checkPlaying(void);

  void setSong(int song);
  int song() const;
  void checkSong(void);
  
  void setRepeat(bool repeat);
  bool repeat() const;
  void checkRepeat(void);
  
  void setShuffle(bool shuffle);
  bool shuffle() const;
  void checkShuffle(void);
  
  void setElapsedTime(unsigned timeMS);
  float elapsedTime() const;
  void checkElapsedTime(void);
  
  void setTotalTime(unsigned timeTotal);
  float totalTime() const;
  void checkTotalTime(void);
  
  QStringList loadUri(void);
  QStringList loadTitle(void);
  void loadQueue(void);
  void checkQueueVersion(void);
  
signals:
  void listmodelChanged(PlayListModel *listmodel);
  void volumeChanged(unsigned volume);
  void playingChanged(unsigned playing);
  void songChanged(int song);
  void repeatChanged(bool repeat);
  void shuffleChanged(bool shuffle);
  void elapsedTimeChanged(float elapsedTime);
  void totalTimeChanged(float totalTime);
  
private slots:
  
private:
  struct mpd_connection *m_connection;
  
  int m_actVol = 0;
  unsigned m_statePlaying = 0;
  int m_song = 0;
  bool m_repeat = false;
  bool m_shuffle = false;
  float m_elapsedTime = 0.0;
  float m_totalTime = 0.0;
  bool onQueueChanged = false;
  unsigned int m_queueVersion = 0;
  
  PlayListModel *m_listmodel = nullptr;
};

#endif
