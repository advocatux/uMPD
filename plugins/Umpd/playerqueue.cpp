/*
 * Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "playerqueue.h"

PlayerQueue::PlayerQueue(QObject *parent) : QObject(parent)
{
}

void PlayerQueue::getConnection(struct mpd_connection *connection)
{
  m_connection = connection;
}

bool PlayerQueue::clear()
{
  bool clear;
  clear = mpd_run_clear(m_connection);
  emit queueChanged();
  return clear;
}

bool PlayerQueue::deletePos(unsigned pos)
{
  bool done = mpd_run_delete(m_connection, pos);
  emit queueChanged();
  return done;
}
