/*
 * Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "playerstate.h"

PlayerState::PlayerState(QObject *parent) : QObject(parent)
{
  auto listmodel = new PlayListModel(this);
  setListmodel(listmodel);
}

void PlayerState::setListmodel(PlayListModel* listmodel)
{
  m_listmodel = listmodel;
}

PlayListModel * PlayerState::listmodel()
{
  return m_listmodel;
}

void PlayerState::getConnection(struct mpd_connection *connection)
{
  m_connection = connection;
  
  //Wiedergabeliste laden
  loadQueue();
  //Lautsärke prüfen
  checkVolume();
  //Prüfen ob Play/Pause/Stopp aktiv ist
  checkPlaying();
  //Abfragen, welcher Titel in der Wiedergabeliste gespielt wird
  checkSong();
  //Abfragen ob repeat gesetzt ist
  checkRepeat();
  //Abfrage ob shuffle gesetzt ist
  checkShuffle();
  //Abfrage der abgelaufenen Zeit des aktuellen Titels
  checkElapsedTime();
  //Abfrage der Gesamtspielzeit für den aktuellen Titel
  checkTotalTime();
}

void PlayerState::update()
{
  //Lautsärke prüfen
  checkVolume();
  //Prüfen ob Play/Pause/Stopp aktiv ist
  checkPlaying();
  //Abfragen, welcher Titel in der Wiedergabeliste gespielt wird
  checkSong();
  //Abfragen ob repeat gesetzt ist
  checkRepeat();
  //Abfrage ob shuffle gesetzt ist
  checkShuffle();
  //Abfrage der abgelaufenen Zeit des aktuellen Titels
  checkElapsedTime();
  //Abfrage der Gesamtspielzeit für den aktuellen Titel
  checkTotalTime();
  //Prüfen ob sich die Wiedergabeliste geändert hat
  checkQueueVersion();
}

double PlayerState::volume() const
{
  return m_actVol;
}

void PlayerState::setVolume(int value)
{
  if(m_actVol != value && value >= 0 && value <= 100)
  {
    m_actVol = value;
    emit volumeChanged(m_actVol);
  }
}

void PlayerState::checkVolume()
{
  //Lautsärke prüfen
  setVolume(mpd_status_get_volume(mpd_run_status(m_connection)));
}

unsigned int PlayerState::playing() const
{
  return m_statePlaying;
}

void PlayerState::setPlaying(unsigned state)
{
  //2 = play, 3 = pause
  if ( state != m_statePlaying ) {
    m_statePlaying = state;
    emit playingChanged(m_statePlaying);
  }  
}

void PlayerState::checkPlaying()
{
  //Prüfen ob Play/Pause/Stopp aktiv ist
  setPlaying(mpd_status_get_state(mpd_run_status(m_connection)));
}

int PlayerState::song() const
{
  return m_song;
}

void PlayerState::setSong(int song)
{
  if(((m_song != song) || onQueueChanged) && (song >= 0))
  {
    m_song = song;
    onQueueChanged = false;
    emit songChanged(m_song);
  }
}

void PlayerState::checkSong()
{
  //Aktuell angewählter Titel
  setSong(mpd_status_get_song_pos(mpd_run_status(m_connection)));
}

bool PlayerState::repeat() const
{
  return m_repeat;
}

void PlayerState::setRepeat(bool repeat)
{
  if(repeat != m_repeat)
  {
    m_repeat = repeat;
    emit repeatChanged(m_repeat);
  }
}

void PlayerState::checkRepeat()
{
  setRepeat(mpd_status_get_repeat(mpd_run_status(m_connection)));
}

bool PlayerState::shuffle() const
{
  return m_shuffle;
}

void PlayerState::setShuffle(bool shuffle)
{
  if(shuffle != m_shuffle)
  {
    m_shuffle = shuffle;
    emit shuffleChanged(m_shuffle);
  }
}

void PlayerState::checkShuffle()
{
  setShuffle(mpd_status_get_random(mpd_run_status(m_connection)));
}

float PlayerState::elapsedTime() const
{
  return m_elapsedTime;
}

void PlayerState::setElapsedTime(unsigned int timeMS)
{
  unsigned int timeSec = timeMS / 1000;
  float timeMod = timeSec % 60;
  float timeMin = timeSec / 60;
  timeMin = timeMin + (timeMod / 100.0);
  
  if(timeMin != m_elapsedTime)
  {
    m_elapsedTime = timeMin;
    emit elapsedTimeChanged(m_elapsedTime);
  }
}

void PlayerState::checkElapsedTime()
{
  setElapsedTime(mpd_status_get_elapsed_ms(mpd_run_status(m_connection)));
}

float PlayerState::totalTime() const
{
  return m_totalTime;
}

void PlayerState::setTotalTime(unsigned int timeTotal)
{
  float timeMod = timeTotal % 60;
  float timeMin = timeTotal / 60;
  timeMin = timeMin + (timeMod / 100.0);

  if(timeMin != m_totalTime)
  {
    m_totalTime = timeMin;
    emit totalTimeChanged(m_totalTime);
  }
}

void PlayerState::checkTotalTime()
{
  setTotalTime(mpd_status_get_total_time(mpd_run_status(m_connection)));
}

void PlayerState::checkQueueVersion()
{
  unsigned int queueVersion = mpd_status_get_queue_version(mpd_run_status(m_connection));
  if(queueVersion != m_queueVersion)
  {
    qDebug() << "New queue version:" << queueVersion;
    m_queueVersion = queueVersion;
    loadQueue();
  }
}

QStringList PlayerState::loadUri(void) {
 
  qDebug() << "Abfrage der Wiedergabeliste";
  onQueueChanged = true;
  
  QStringList playList;
  struct mpd_entity *entity;
  const struct mpd_song *song;
  
  mpd_send_list_queue_meta(m_connection);

  while (entity = mpd_recv_entity(m_connection)) {
      
    if (mpd_entity_get_type(entity) == MPD_ENTITY_TYPE_SONG)
    {
      song = mpd_entity_get_song(entity);
      playList << mpd_song_get_uri(song);
    }
      
    mpd_entity_free(entity);
      
  }
  
  if( !mpd_response_finish(m_connection)) qDebug() << "error on response";
  
  qDebug() << playList;
  
  return playList;
}

QStringList PlayerState::loadTitle(void) {
 
  qDebug() << "Abfrage der Titel";
  
  QStringList title;
  struct mpd_entity *entity;
  const struct mpd_song *song;
  
  mpd_send_list_queue_meta(m_connection);

  while (entity = mpd_recv_entity(m_connection)) {
      
    if (mpd_entity_get_type(entity) == MPD_ENTITY_TYPE_SONG)
    {
      song = mpd_entity_get_song(entity);
      title << mpd_song_get_tag(song, MPD_TAG_TITLE, 0);
    }
      
    mpd_entity_free(entity);
      
  }
  
  if( !mpd_response_finish(m_connection)) qDebug() << "error on response";
  
  qDebug() << title;
    
  return title;
}

void PlayerState::loadQueue()
{
  m_listmodel->loadQueue(loadUri(), loadTitle());
}
