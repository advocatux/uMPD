/*
 * Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "umpd.h"

Umpd::Umpd()
{
  auto status = new PlayerState(this);
  setStatus(status);
  
  auto control = new PlayerControl(this);
  setControl(control);
  
  auto queue = new PlayerQueue(this);
  setQueue(queue);
  
  auto playlist = new PlayList(this);
  setPlaylist(playlist);
}

void Umpd::setStatus(PlayerState* status)
{
  m_status = status;
}

PlayerState * Umpd::status()
{
  return m_status;
}

void Umpd::setControl(PlayerControl* control)
{
  m_control = control;
  connect(m_control, &PlayerControl::volumeChanged,
          this, &Umpd::onVolumeChanged);
  connect(m_control, &PlayerControl::playingChanged,
          this, &Umpd::onPlayingChanged);
  connect(m_control, &PlayerControl::songChanged,
          this, &Umpd::onSongChanged);
  connect(m_control, &PlayerControl::repeatChanged,
          this, &Umpd::onRepeatChanged);
  connect(m_control, &PlayerControl::shuffleChanged,
        this, &Umpd::onShuffleChanged);
  connect(m_control, &PlayerControl::seekedPos,
        this, &Umpd::onSeekPosChanged);
}

PlayerControl * Umpd::control()
{
  return m_control;
}

void Umpd::setQueue(PlayerQueue* queue)
{
  m_queue = queue;
  connect(m_queue, &PlayerQueue::queueChanged,
          this, &Umpd::onQueueChanged);
}

PlayerQueue * Umpd::queue()
{
  return m_queue;
}

void Umpd::setPlaylist(PlayList* playlist)
{
  m_playlist = playlist;
}

PlayList * Umpd::playlist()
{
  return m_playlist;
}

//Daten für die Verbindung
//IP: 192.168.1.151
//Port: 6600
//Timeout: 0ms

bool Umpd::newConnection(QString ip, unsigned int port, unsigned int timeout_ms, QString password)
{
  bool accepted = false;
  QByteArray baip = ip.toLocal8Bit();
  const char *host = baip.data();
  
  connection = mpd_connection_new(host, port, timeout_ms);
  enum mpd_error error = mpd_connection_get_error(connection);
  qDebug() << error;
  
  if( error == MPD_ERROR_SUCCESS) //Verbindung erfolgreich aufgebaut
  {
    //send password
    if( password != "") {
      QByteArray bapasswd = password.toLocal8Bit();
      const char *passwd = bapasswd.data();
      accepted = mpd_run_password(connection, passwd);
      qDebug() << accepted << "status login";
    } else {
      qDebug() << "Verbinden ohne Kennwort";
      if(mpd_run_status(connection) != NULL) {
        accepted = true;
      } else {
        m_strError = tr("Successful connection\nNo read permission");
        emit strErrorChanged(m_strError);
        freeConnection();
        
        return false;
      }
    }
  } else if( error == MPD_ERROR_SYSTEM) {

    qDebug() << "system error"; //Systemfehler
    m_strError = tr("Connection failed\nPlease check connection settings");
    emit strErrorChanged(m_strError);
    
    return false;
    
  }

  if(accepted) {
    
    m_strError = "";
    emit strErrorChanged(m_strError);
    
    m_status->getConnection(connection);
    m_control->getConnection(connection);
    m_queue->getConnection(connection);
    m_playlist->getConnection(connection);

    setStatusConnection(error == MPD_ERROR_SUCCESS);
    
    return error == MPD_ERROR_SUCCESS;
    
  } else {

    m_strError = tr("Connection failed\nPlease check the password");
    emit strErrorChanged(m_strError);
    
    freeConnection();
    return false;
    
  }

}

void Umpd::freeConnection(void)
{
  mpd_connection_free(connection);
  setStatusConnection(false);
  qDebug() << "Verbindung wieder getrennt";
}

QString Umpd::strError() const
{
 return m_strError;
}

void Umpd::onPlayingChanged()
{
  m_status->checkPlaying();
}

void Umpd::onVolumeChanged()
{
  m_status->checkVolume();
}

void Umpd::onSongChanged()
{
  m_status->checkSong();
}

void Umpd::onRepeatChanged()
{
  m_status->checkRepeat();
}

void Umpd::onShuffleChanged()
{
  m_status->checkShuffle();
}

void Umpd::onSeekPosChanged()
{
  m_status->checkElapsedTime();
}

void Umpd::onQueueChanged()
{
  m_status->checkQueueVersion();
}

void Umpd::setStatusConnection(bool status) {
    if ( status != m_connected) {
      m_connected = status;
      emit statusConnectionChanged(m_connected); //set status connected
    }
}

bool Umpd::connected() const
{
    qDebug() << m_connected << "Status der Verbindung";
    return m_connected;
}
