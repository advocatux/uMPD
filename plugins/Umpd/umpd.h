/*
 * Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UMPD_H
#define UMPD_H

#include <QObject>
#include <QDebug>
#include <QStringList>
#include <QQuickView>
#include <QStringList>
#include <QQmlContext>
#include <libintl.h>

#include "mpd/client.h"
#include "playerstate.h"
#include "playerqueue.h"
#include "playercontrol.h"
#include "playlist.h"

class Umpd: public QObject {
  Q_OBJECT
  Q_PROPERTY(bool connected READ connected WRITE setStatusConnection NOTIFY statusConnectionChanged)
  
  Q_PROPERTY(PlayerState *status READ status WRITE setStatus NOTIFY statusChanged)
  Q_PROPERTY(PlayerControl *control READ control WRITE setControl NOTIFY controlChanged)
  Q_PROPERTY(PlayerQueue *queue READ queue WRITE setQueue NOTIFY queueChanged)
  Q_PROPERTY(PlayList *playlist READ playlist WRITE setPlaylist NOTIFY playlistChanged)
  Q_PROPERTY(QString strError READ strError NOTIFY strErrorChanged)
    
public:
  Umpd();
  ~Umpd() = default;
  
  PlayerState *status();
  void setStatus(PlayerState *status);
  
  PlayerControl *control();
  void setControl(PlayerControl *control);
  
  PlayerQueue *queue();
  void setQueue(PlayerQueue *queue);

  PlayList *playlist();
  void setPlaylist(PlayList *playlist);
  
  Q_INVOKABLE bool newConnection(QString ip, unsigned port, unsigned time_ms, QString password);
  Q_INVOKABLE void freeConnection(void);

  void setStatusConnection(bool status); 
    
  void getStatus();
    
  bool connected() const;
  
  QString strError() const;
 
signals:
  void statusConnectionChanged(bool connected);
  
  void statusChanged(PlayerState *status);
  void controlChanged(PlayerControl *control);
  void queueChanged(PlayerQueue *queue);
  void playlistChanged(PlayList *playlist);
  void strErrorChanged(QString strError);
  
private slots:
  void onVolumeChanged();
  void onPlayingChanged();
  void onSongChanged();
  void onQueueChanged();
  void onRepeatChanged();
  void onShuffleChanged();
  void onSeekPosChanged();
  void onPlaylistsChanged();
 
private:
  struct mpd_connection *connection;
  bool m_connected = false;
  QString m_strError = "";

  PlayerState *m_status = nullptr;
  PlayerControl *m_control = nullptr;
  PlayerQueue *m_queue = nullptr;
  PlayList *m_playlist = nullptr;
  
};

#endif
