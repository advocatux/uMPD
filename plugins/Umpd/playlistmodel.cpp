/*
 * Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "playlistmodel.h"
//-------------------------------------------------------------------------------------------------

PlayListModel::PlayListModel(QObject *parent) : QAbstractListModel(parent)
{

}
//-------------------------------------------------------------------------------------------------
int PlayListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_textItems.size();

}
//-------------------------------------------------------------------------------------------------
QVariant PlayListModel::data(const QModelIndex &index, int role) const
{
    if(m_textItems.size() != 0)
    {
        if (role == Qt::UserRole)
        {
            return QVariant(m_textItems[index.row()]);
        }
        if (role == Qt::UserRole+1)
        {
            return QVariant(m_otherTextItems[index.row()]);
        }
        if (role == Qt::UserRole+2)
        {
            return QVariant(m_otherOtherItems[index.row()]);
        }
        
        return QVariant();
    }
    return QVariant();

}

QHash<int,QByteArray> PlayListModel::roleNames() const {
  
  QHash<int, QByteArray> roles;
  roles[Qt::UserRole]="uid";
  roles[Qt::UserRole+1]="title";
  roles[Qt::UserRole+2]="uri";
  
  return roles;
}

//-------------------------------------------------------------------------------------------------
void PlayListModel::appendRow(QString textData, QString otherTextData, QString anotherTextData)
{
    int lastElemPos = m_textItems.size();
    beginInsertRows(QModelIndex(), lastElemPos, lastElemPos);
    m_textItems << textData;
    m_otherTextItems << otherTextData;
    m_otherOtherItems << anotherTextData;
    
    endInsertRows();
}
//-------------------------------------------------------------------------------------------------
void PlayListModel::myRemoveRows(int position, int rows)
{
    beginRemoveRows(QModelIndex(), position, position+rows-1);
    for (int row=0; row < rows; ++row) 
    {
        m_textItems.removeAt(position);
        m_otherTextItems.removeAt(position);
        m_otherOtherItems.removeAt(position);
    }

    endRemoveRows();    
}
//-------------------------------------------------------------------------------------------------
void PlayListModel::addAnItem(QVariant textItem, QVariant otherTextItem, QVariant anotherTextItem)
{
    appendRow(textItem.toString(), otherTextItem.toString(), anotherTextItem.toString());
}
//-------------------------------------------------------------------------------------------------
void PlayListModel::removeLastItem()
{
    if(m_textItems.size() > 0)
    {
        int lastElemPos = m_textItems.size() - 1;    
        myRemoveRows(lastElemPos, 1);
    }
}

void PlayListModel::removeAllItems()
{
  while(m_textItems.size() > 0)
  {
    int lastElemPos = m_textItems.size() - 1;    
    myRemoveRows(lastElemPos, 1);
  }
}

void PlayListModel::loadQueue(QStringList uriList, QStringList titleList) {
  
  removeAllItems();
  
  qDebug() << "Load actual queue";
  unsigned i=0;

  for(QString item: uriList) {  //playList.count() = gibt die Länge von QStringList zurück
    appendRow(QString::number(i), titleList.value(i), item);
    i++;
  }
  
}

void PlayListModel::loadPlaylists(QStringList playlists) {
  
  removeAllItems();
  
  qDebug() << "Load Playlists";
  unsigned i=0;
  
  for(QString item: playlists) {  //playList.count() = gibt die Länge von QStringList zurück
    appendRow(QString::number(i), item, "");
    i++;
  }
}
