/*
 * Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.1
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3
import "controller"
import Umpd 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'umpd.stefanweng'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    PageStack {
      id: pageStack
        
      anchors {
        left: parent.left
        right: parent.right
        top: parent.top
        bottom: parent.top
      }
      
      SettingsController { id: settings }
      
      Component.onCompleted: {
        if ( Umpd.newConnection(settings.address, settings.port, settings.timeout_ms, settings.password) == true ) {
          pageStack.push( Qt.resolvedUrl("./pages/MainPage.qml") )
        }
        else {
          pageStack.push( Qt.resolvedUrl("./pages/SettingsPage.qml") )
        }
      }
    }
}
