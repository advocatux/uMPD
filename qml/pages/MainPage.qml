/*
 * Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Umpd 1.0

Page {
    id: mainPage

    property var queue: 0
    
    header: PageHeader {
        id: header
        title: "uMPD"
        
        leadingActionBar {
            id: menu
            actions : [
                Action {
                    id: settingsIcon
                    objectName: "settingsIcon"
                    text: i18n.tr("Settings")
                    iconName: "settings"
                    onTriggered: {
                        push( Qt.resolvedUrl("./SettingsPage.qml") )
                    }
                },
                Action {
                    id: databaseIcon
                    objectName: "editIcon"
                    text: i18n.tr("Playlists")
                    iconName: "edit"
                    onTriggered: {
                        push( Qt.resolvedUrl("./PlaylistsPage.qml") )
                    }
                }
            ]
            numberOfSlots: 1
        }

        
        trailingActionBar.actions: [
            Action {
                id: infoIcon
                objectName: "infoIcon"
                text: i18n.tr("About")
                iconName: "info"
                onTriggered: {
                  push( Qt.resolvedUrl("./AboutPage.qml") )
                }
            },
            Action {
                id: clearIcon
                objectName: "clearIcon"
                text: i18n.tr("Clear Queue")
                iconName: "edit-delete"
                onTriggered: {
                  PopupUtils.open(clearQueue)
                }
            },
            Action {
                id: saveQeue
                objectName: "saveAsIcon"
                text: i18n.tr("Save Queue")
                iconName: "save-as"
                onTriggered: {
                  PopupUtils.open(saveQueue)
                }
            }
        ]

        StyleHints {
            foregroundColor: "#FFF"
            backgroundColor: UbuntuColors.orange
            dividerColor: "#85D8CE"
        }
        
    }

    Timer {
        interval: 1000
        running: Umpd.connected
        repeat: true
        onTriggered: Umpd.status.update()
    }
    
    Component {
        id: clearQueue
	
        Dialog {
            id: clearQueueDialog
            title: i18n.tr("Queue")
           
            Label { 
                text: i18n.tr("Are you sure you want to clear the queue?")
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }
           
            Button {
                 text: i18n.tr("ok")
                 color: UbuntuColors.red
                 onClicked:
                 {
                   Umpd.queue.clear()
                   PopupUtils.close(clearQueueDialog)
                 }
            }
            
            Button {
                 text: i18n.tr("cancel")
                 color: UbuntuColors.ash
                 onClicked: PopupUtils.close(clearQueueDialog)
            }
        }
    }

    Component {
        id: saveQueue
	
        Dialog {
            id: saveQueueDialog
            title: i18n.tr("Save queue as playlist")
           
            Label { 
                text: i18n.tr("Please enter the name under you want to save the queue")
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }
           
            TextField {
                id: fieldQueueName
                horizontalAlignment: Text.AlignHCenter
            }
           
            Button {
                text: i18n.tr("save")
                color: UbuntuColors.green
                onClicked:
                {
                  Umpd.playlist.saveAs(fieldQueueName.text)
                  PopupUtils.close(saveQueueDialog)
                }
            }
            
            Button {
                text: i18n.tr("cancel")
                color: UbuntuColors.ash
                onClicked: PopupUtils.close(saveQueueDialog)
            }
        }
    }

    ListView {
      id: playListView
      width: mainPage.width
      height: mainPage.height - header.height - setPlayTimeSlider.height - itemButton.height - iconVolumeLow.height - units.gu(6)
      anchors.top: header.bottom
      currentIndex: Umpd.status.song
    
      model: Umpd.status.listmodel
      
      delegate: ListItem {

        //width: parent.width
        //height: layout.height
        //color: chat.isSecret ? "lightgreen" : "transparent"

        height: modelLayout.height + (divider.visible ? divider.height : 0)
      
        ListItemLayout {
          id: modelLayout
            Column {
                id: column
                //anchors { fill: parent; margins: 2 }
                //Text { text: 'Name: ' + name }
                //Text { text: 'Type: ' + type }
                Text { text: model.title }
                Text { text: model.uid + ": " + model.uri }
            }
          //title.text: model.uid + ": " + model.uri
        }

        MouseArea {
          anchors.fill: parent
          onClicked: Umpd.control.playPos(model.uid)
        }

        leadingActions: ListItemActions {
          actions: [
            Action {
              iconName: "delete"
              text: i18n.tr("delete")
              onTriggered: Umpd.queue.deletePos(model.uid)
            }
          ]
        }
      }
      
      highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
      highlightMoveDuration: 10
      highlightMoveVelocity: -1
      focus: true
     
    }
    
    Rectangle {
      anchors.top: setPlayTimeSlider.top
      width: parent.width
      height: units.gu(20)
      color: "white"
    }

    Item {
      id: setPlayTimeSlider
      anchors.top: playListView.bottom
      width: parent.width
      
      Slider {
        id: progressSlider
        width: parent.width
        function formatValue(v) { return v.toFixed(2) }
        minimumValue: 0.0
        maximumValue: Umpd.status.totalTime
        value: Umpd.status.elapsedTime // load value at startup
        live: false
        
        onPressedChanged: {
          if(!pressed) {
            Umpd.control.playSeekPos(Umpd.status.song, value)
          }
        }
      }
    }
    
    Binding {
      target: progressSlider
      property: "value"
      value: Umpd.status.elapsedTime
      when: !progressSlider.pressed
    }

    Item {
      id: itemButton
      anchors.top: setPlayTimeSlider.bottom
      anchors.topMargin: units.gu(4.5)
      
      width: parent.width
      height: units.gu(5)
      
      property var widthButton: parent.width / 8

      Row { // The "Row" type lays out its child items in a horizontal line
        spacing: (parent.width - (itemButton.widthButton * 6)) / 7 // Places space between items
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        
        Button {
          id: buttonBack
          
          color: UbuntuColors.orange

          width: itemButton.widthButton
          height: units.gu(5)

          iconName: "media-skip-backward"
          
          onClicked: {
            Umpd.control.playPrevious()
          }
        }

        Button {
          id: buttonPlayPause
          
          color: UbuntuColors.orange

          width: itemButton.widthButton
          height: units.gu(5)

          iconName: Umpd.status.playing === 2 ? "media-playback-pause" : "media-playback-start"
          
          onClicked: {
            Umpd.control.play(Umpd.status.playing)
          }
        }

        Button {
          id: buttonStop
          
          color: UbuntuColors.orange

          width: itemButton.widthButton
          height: units.gu(5)

          iconName: "media-playback-stop"      
          
          onClicked: {
            Umpd.control.stop()
          }
        }

        Button {
          id: buttonForward
          
          color: UbuntuColors.orange
          
          width: itemButton.widthButton
          height: units.gu(5)

          iconName: "media-skip-forward"      
          
          onClicked: {
            Umpd.control.playNext()
          }
        }

        Button {
          id: buttonRepeat

          color: Umpd.status.repeat ? UbuntuColors.green : UbuntuColors.orange

          width: itemButton.widthButton
          height: units.gu(5)
          
          iconName: "media-playlist-repeat-one"

          onClicked: {
            Umpd.control.repeat(Umpd.status.repeat)
          }
        }

        Button {
          id: buttonShuffle

          color: Umpd.status.shuffle ? UbuntuColors.green : UbuntuColors.orange

          width: itemButton.widthButton
          height: units.gu(5)
          
          iconName: "media-playlist-shuffle"

          onClicked: {
            Umpd.control.shuffle(Umpd.status.shuffle)
          }
        }
      }
    }
    
    Label {
      id: labelActVolume
      width: units.gu(3)
      height: units.gu(3)
      anchors.top: itemButton.bottom
      anchors.topMargin: units.gu(1.4)
      anchors.leftMargin: units.gu(1)
      anchors.left: parent.left
      text: Umpd.status.volume
    }
    
    Icon {
      id: iconVolumeLow
      anchors.top: itemButton.bottom
      anchors.topMargin: units.gu(1)
      anchors.left: labelActVolume.right
      width: units.gu(3)
      height: units.gu(3)
      name: "audio-volume-low"
    }
    
    Icon {
      id: iconVolumeHigh
      anchors.top: itemButton.bottom
      anchors.topMargin: units.gu(1)
      anchors.left: setVolumeSlider.right
      width: units.gu(3)
      height: units.gu(3)
      name: "audio-volume-high"
    }
    
    Item {
      id: setVolumeSlider
      anchors.top: itemButton.bottom
      anchors.left: iconVolumeLow.right
      
      width: parent.width - labelActVolume.width - iconVolumeLow.width - iconVolumeHigh.width - units.gu(2)
      
      Slider {
        id: volumeSlider
        width: parent.width
        minimumValue: 0
        maximumValue: 100
        value: Umpd.status.volume
        live: false

        onPressedChanged: {
          if(!pressed) {
            Umpd.control.setVolume(value)
          }
        }
      }
    }
    
    Binding {
      target: volumeSlider
      property: "value"
      value: Umpd.status.volume
      when: !volumeSlider.pressed
    } 

}
