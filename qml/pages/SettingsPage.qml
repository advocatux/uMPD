/*
 * Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.1
import Ubuntu.Components 1.3
import Umpd 1.0

Page {
    id: settingsPage
    
    header: PageHeader {
        id: pageHeader
        title: i18n.tr('Settings')

        leadingActionBar.actions: [
            Action {
                visible: Umpd.connected
                iconName: "back"
                text: i18n.tr('Back')
                onTriggered: {
                    pageStack.pop()
                }                
            }
        ]
        
        trailingActionBar.actions: [
            Action {
                id: accountIcon
                objectName: "infoIcon"
                text: i18n.tr("About")
                iconName: "info"
                onTriggered: {
                  push( Qt.resolvedUrl("./AboutPage.qml") )
                }
            }
        ]
        
        StyleHints {
            foregroundColor: "#FFF"
            backgroundColor: UbuntuColors.orange
            dividerColor: "#85D8CE"
        }

    }

    Label {
        id: labelIpAdd
        anchors.top: pageHeader.bottom
        anchors.topMargin: units.gu(2)
        anchors.left: parent.left
        anchors.leftMargin: units.gu(2)
        text: i18n.tr('Server Ip Address:')
    }
    
    TextField {
        id: fieldIpAdd
        anchors.top: pageHeader.bottom
        anchors.topMargin: units.gu(1) 
        anchors.left: settingsPage.horizontalCenter
        anchors.leftMargin: units.gu(-5)
        inputMethodHints: Qt.ImhDigitsOnly
        maximumLength : 15
        text: settings.address
        
        onAccepted: settings.address = text
    }

    Label {
        id: labelPort
        anchors.top: fieldIpAdd.bottom
        anchors.topMargin: units.gu(2)
        anchors.left: parent.left
        anchors.leftMargin: units.gu(2)
        text: i18n.tr('Port number:')
    }
    
    TextField {
        id: fieldPort
        anchors.top: fieldIpAdd.bottom
        anchors.topMargin: units.gu(1) 
        anchors.left: settingsPage.horizontalCenter
        anchors.leftMargin: units.gu(-5)
        inputMethodHints: Qt.ImhDigitsOnly
        maximumLength : 5
        text: settings.port
        
        onAccepted: settings.port = text
        
    }
    
    Label {
        id: labelTimeout
        anchors.top: fieldPort.bottom
        anchors.topMargin: units.gu(2)
        anchors.left: parent.left
        anchors.leftMargin: units.gu(2)
        text: i18n.tr('Timeout (ms):')
    }
    
   TextField {
        id: fieldTimeout
        anchors.top: fieldPort.bottom
        anchors.topMargin: units.gu(1) 
        anchors.left: settingsPage.horizontalCenter
        anchors.leftMargin: units.gu(-5)
        inputMethodHints: Qt.ImhDigitsOnly
        maximumLength : 5
        text: settings.timeout_ms
        
        onAccepted: settings.timeout_ms = text
        
    }
    
    Label {
        id: labelPassword
        anchors.top: fieldTimeout.bottom
        anchors.topMargin: units.gu(2)
        anchors.left: parent.left
        anchors.leftMargin: units.gu(2)
        text: i18n.tr('Password:')
    }

   TextField {
        id: fieldPassword
        anchors.top: fieldTimeout.bottom
        anchors.topMargin: units.gu(1) 
        anchors.left: settingsPage.horizontalCenter
        anchors.leftMargin: units.gu(-5)
        echoMode: TextInput.Password
        text: settings.password
        
        onAccepted: settings.password = text
        
    }
    
    Button {
      id: buttonConnect
      anchors.top: fieldPassword.bottom
      anchors.topMargin: units.gu(2)
      anchors.horizontalCenter: settingsPage.horizontalCenter

      text: Umpd.connected ? i18n.tr('Disconnect') : i18n.tr('Connect')
      
      font.pointSize: units.gu(1.4)
      color: Umpd.connected ? UbuntuColors.red : UbuntuColors.green

      width: units.gu(12)
      height: units.gu(6)

      onClicked: {
        if (Umpd.connected) {
          Umpd.freeConnection()
        }
        else if ( Umpd.newConnection(settings.address, settings.port, settings.timeout_ms, settings.password) == true ) {
          pageStack.push( Qt.resolvedUrl("./MainPage.qml") )
        }
      }
    }
    
    Label {
        id: labelError
        width: parent.width
        anchors.top: buttonConnect.bottom
        anchors.topMargin: units.gu(3)
        
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter 
        
        visible: Umpd.strError != "" ? true : false
        
        font.pointSize: units.gu(2)
        color: "red"
        text: Umpd.strError
    }

}
