/*
 * Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Umpd 1.0

Page {
    id: databasePage
    
    header: PageHeader {
        id: header
        title: i18n.tr('Playlists')

        leadingActionBar.actions: [
            Action {
                iconName: "back"
                text: i18n.tr('Back')
                onTriggered: {
                    pageStack.pop()
                }
            }
        ]

        trailingActionBar.actions: [
            Action {
                id: accountIcon
                objectName: "infoIcon"
                text: i18n.tr("About")
                iconName: "info"
                onTriggered: {
                  push( Qt.resolvedUrl("./AboutPage.qml") )
                }
            }
        ]
        
        StyleHints {
            foregroundColor: "#FFF"
            backgroundColor: UbuntuColors.orange
            dividerColor: "#85D8CE"
        }
        
    }
          
    Component {
        id: removePlaylist
	
        Dialog {
            id: removePlaylistDialog
            title: i18n.tr("Remove playlist")
           
            Label { 
                text: i18n.tr("Are you sure you want to remove the selected playlist from the server?")
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }
           
            Button {
                text: i18n.tr("ok")
                color: UbuntuColors.red
                onClicked:
                {
                  Umpd.playlist.remove()
                  PopupUtils.close(removePlaylistDialog)
                }
            }
            
            Button {
                text: i18n.tr("cancel")
                color: UbuntuColors.ash
                onClicked: PopupUtils.close(removePlaylistDialog)
            }
        }
    }
          
    ListView {
        id: playlistsView
        width: parent.width
        height: parent.height - header.height
        anchors.top: header.bottom
        currentIndex: Umpd.playlist.selPlaylist
            
        model: Umpd.playlist.playmodel
              
        delegate: ListItem {

            height: modelLayout.height + (divider.visible ? divider.height : 0)
              
            ListItemLayout {
                id: modelLayout
                    Column {
                        id: column
                        Text { text: model.uid + ": " + model.title }
                    }
                }

            MouseArea {
                anchors.fill: parent
                  onPressed: Umpd.playlist.selectedEntity(model.title, model.uid)
                }

            leadingActions: ListItemActions {
                actions: [
                  Action {
                      iconName: "delete"
                      text: i18n.tr("delete")
                      onTriggered: PopupUtils.open(removePlaylist)
                    }
                  ]
                }
                
            trailingActions: ListItemActions {
                actions: [
                    Action {
                        iconName: "media-playback-start"
                        text: i18n.tr("Play")
                        onTriggered: {
                          Umpd.queue.clear()
                          if(Umpd.playlist.addToQueue()) {
                            rectUserInfo.userInfoText = i18n.tr("Play playlist")
                            Umpd.control.play(1)
                          } else {
                            rectUserInfo.userInfoText = i18n.tr("Playlist could not be played back")
                          }
                          infoTimer.start()
                          rectUserInfo.showLabel = true                          
                        }
                    },
                    Action {
                        iconName: "media-playlist"
                        text: i18n.tr("Add")
                        onTriggered: {
                          if(Umpd.playlist.addToQueue()) {
                            rectUserInfo.userInfoText = i18n.tr("Playlist add to the queue")
                          } else {
                            rectUserInfo.userInfoText = i18n.tr("Playlist could not be added to playlist")
                          }
                          infoTimer.start()
                          rectUserInfo.showLabel = true
                        }
                    }
                ]
            }
        }
        highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
        highlightMoveDuration: 10
        highlightMoveVelocity: -1
        focus: true
    }

    Timer {
        id: infoTimer 
        interval: 2000 //ms
        repeat: false
        onTriggered: rectUserInfo.showLabel = false
    }

    Rectangle {
        id: rectUserInfo
        
        color: UbuntuColors.warmGrey
        
        width: labelUserInfo.contentWidth + units.gu(3)
        height: labelUserInfo.contentHeight + units.gu(1)
        
        radius: units.gu(1)
        
        border.width: units.gu(0.1)
        border.color: UbuntuColors.graphite
        
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        
        property var showLabel: false
        property var userInfoText: ""

        visible: showLabel
        
        Label {
            id: labelUserInfo
            
            width: contentWidth
            
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter 
            
            visible: rectUserInfo.showLabel
            
            font.pointSize: units.gu(2)
            color: UbuntuColors.porcelain
            text: rectUserInfo.userInfoText
        }
    }
}
