/*
 * Copyright (C) 2019 - Stefan Weng <stefan.weng@gmx.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

Page {
    id: aboutPage

    property string version: "@APP_VERSION@"

    header: PageHeader {
        title: i18n.tr('About')
        flickable: aboutFlickable

        leadingActionBar.actions: [
            Action {
                iconName: "back"
                text: i18n.tr('Back')
                onTriggered: {
                    pageStack.pop()
                }
            }
        ]

        extension: Sections {
            id: sections
            anchors {
                horizontalCenter: parent.horizontalCenter
            }

            model: [
                i18n.tr("About"),
                i18n.tr("Support")
            ]

            onSelectedIndexChanged: tabView.currentIndex = selectedIndex

            StyleHints {
                sectionColor: "#88FFFFFF"
                selectedSectionColor: "White"
                underlineColor: "Transparent"
                pressedBackgroundColor: "#76C8C4"
            }
        }

        StyleHints {
            foregroundColor: "#FFF"
            backgroundColor: UbuntuColors.orange
            dividerColor: "#85D8CE"
        }
        
    }

    VisualItemModel {
        id: tabs

        Item {
            width: tabView.width
            height: tabView.height
            opacity: tabView.currentIndex === 0 ? 1 : 0

            Behavior on opacity {
                NumberAnimation { duration: 200; easing.type: Easing.InOutCubic }
            }
    
            Flickable {
                id: aboutFlickable
                anchors.fill: parent
                anchors.margins: units.gu(2)

                Column {
                    id: aboutCloumn
                    spacing: units.gu(2)
                    width:parent.width

                    UbuntuShape {
                        width: units.gu(15); height: units.gu(15)
                        anchors.horizontalCenter: parent.horizontalCenter
                        radius: "medium"
                        image: Image {
                            source: Qt.resolvedUrl("../../assets/logo.svg")
                        }
                    }

                    Label {
                        width: parent.width
                        font.pixelSize: units.gu(5)
                        font.bold: true
                        color: theme.palette.normal.backgroundText
                        horizontalAlignment: Text.AlignHCenter
                        text: i18n.tr("uMPD")
                    }
                    
                    Label {
                        width: parent.width
                        color: UbuntuColors.ash
                        horizontalAlignment: Text.AlignHCenter
                        text: i18n.tr("Version %1").arg(version)
                    }
                    
                    Label {
                        width: parent.width
                        horizontalAlignment: Text.AlignHCenter
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        text: i18n.tr("This application lets you control a music player daemon")
                    }
            
                    Label {
                        width: parent.width
                        linkColor: UbuntuColors.orange
                        horizontalAlignment: Text.AlignHCenter
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        //TRANSLATORS: Please make sure the URLs are correct
                        text: i18n.tr("Released under the terms of the <a href=\"https://gitlab.com/StefWe/uMPD/blob/master/LICENSE\">GNU GPL v3</a>")
                        onLinkActivated: Qt.openUrlExternally(link)
                    }
                    
                    Label {
                        width: parent.width
                        linkColor: UbuntuColors.orange
                        horizontalAlignment: Text.AlignHCenter
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        style: Font.Bold
                        text: i18n.tr("Copyright") + " (c) 2019 Stefan Weng"
                    }
                }
            }
        }

        Item {
            width: tabView.width
            height: tabView.height
            opacity: tabView.currentIndex === 1 ? 1 : 0
            Behavior on opacity {
                NumberAnimation { duration: 200; easing.type: Easing.InOutCubic }
            }
        
            UbuntuListView {
                id: listViewAbout
                anchors.fill: parent

                currentIndex: -1
                interactive: false

                model: [
                    { name: i18n.tr("Get the source"), url: "https://gitlab.com/stefwe/umpd" },
                    { name: i18n.tr("Report issues"),  url: "https://gitlab.com/stefwe/umpd/issues" },
                    { name: i18n.tr("Help translate"), url: "https://gitlab.com/StefWe/umpd/tree/master/po" },
                    { name: "Music Player Daemon", url: "https://www.musicpd.org" }
                ]

                delegate: ListItem {
                    ListItemLayout {
                        title.text : modelData.name
                        Icon {
                            width:units.gu(2)
                            name:"go-to"
                        }
                    }
                    onClicked: Qt.openUrlExternally(modelData.url)
                }
            }
        }
    }
    
    ListView {
        id: tabView
        anchors {
            top: parent.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        model: tabs
        orientation: Qt.Horizontal
        snapMode: ListView.SnapOneItem
        highlightRangeMode: ListView.StrictlyEnforceRange
        highlightMoveDuration: UbuntuAnimation.FastDuration

        onCurrentIndexChanged: {
            sections.selectedIndex = currentIndex
        }
    }
}
