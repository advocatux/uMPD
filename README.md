[![pipeline status](https://gitlab.com/StefWe/uMPD/badges/master/pipeline.svg)](https://gitlab.com/StefWe/uMPD/commits/master)

<img width="200px" src="assets/logo.png" />

# uMPD

Is an Music Player Daemon Client for your Ubuntu Touch device.

Based on the library libmpdclient version 2.9

[Music Player Daemon](https://www.musicpd.org)

## How to get it

To download visit [uMPD app at OpenStore](https://open-store.io/app/umpd.stefanweng). Or build it yourself following instructions below.

## Building the app

### Using clickable
To build and run the app on the desktop run:

```
clickable desktop
```
To build and install a click package on the device run:

```
clickable
```

See [clickable documentation](http://clickable.bhdouglass.com/en/latest/) for details.

## License

Copyright (C) 2018 Stefan Weng

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
